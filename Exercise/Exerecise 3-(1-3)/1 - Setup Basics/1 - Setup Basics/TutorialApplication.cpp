/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h> 

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	spd = 2;
	acceleration = 0;
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	ManualObject* object = mSceneMgr->createManualObject("Object1");
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	int triangle[12 * 3][3] = {
		{-5, 5, 5}, {-5, -5, 5}, {5, -5, 5},
		{-5, 5, 5}, {5, -5, 5}, {5, 5, 5},
		{5, 5, 5}, {5, -5, 5},{5, 5, -5},
		{5,5,-5}, {5, -5, 5}, {5, -5, -5},
		{5, 5, -5}, {5, -5, -5}, {-5,-5, -5},
		{5, 5, -5}, {-5,-5, -5}, {-5, 5, -5},
		{-5, 5, -5}, {-5, -5, -5}, {-5, 5, 5},
		{-5, -5, -5}, {-5, -5, 5}, {-5, 5, 5},
		{5, -5, 5}, {-5, -5, -5}, {5, -5, -5},
		{5, -5, 5}, {-5, -5, 5}, {-5, -5, -5},
		{-5, 5, 5}, {5, 5, 5}, {-5, 5, -5},
		{-5, 5, -5},{5, 5, 5}, {5, 5, -5}
	};

	for (auto& x : triangle) {
		for (auto& y : x) {
			y *= 2;
		}
	}

	for (int i = 0; i < 12 * 3; i++) {
		object->position(triangle[i][0], triangle[i][1], triangle[i][2]);
		object->colour(rand() % 5, rand() % 5, 1);
	}



	object->end();
	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(); 
	cubeNode->attachObject(object);
}


bool TutorialApplication::frameStarted(const FrameEvent &evt)
{
	bool isPress = false;
	if (mKeyboard->isKeyDown(OIS::KC_I)) {
		cubeNode->translate(0, 0, spd * acceleration);
		isPress = true;
	}
	if (mKeyboard->isKeyDown(OIS::KC_J)) {
		cubeNode->translate(-spd * acceleration, 0, 0);
		isPress = true;
	}
	if (mKeyboard->isKeyDown(OIS::KC_K)) {
		cubeNode->translate(0, 0, -spd * acceleration);
		isPress = true;
	}
	if (mKeyboard->isKeyDown(OIS::KC_L)) {
		cubeNode->translate(spd * acceleration, 0, 0);
		isPress = true;
	}
	if (!isPress) {
		acceleration = 0;
	}
	else {
		if (acceleration < 0.85f)
		acceleration += evt.timeSinceLastFrame;
	}

	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
