#pragma once
#include<iostream>
#include<string>

using namespace std;
class Characters;
class Skill {
public:
	Skill();
	int getMpCost();
	string getName();
	virtual void Cast(Characters* target, Characters * user);
protected:
	int mpCost;
	string name;
};