#pragma once
#include"Skill.h"

class Vengeance : public Skill
{
public:
	Vengeance(int mp, string name);
	void Cast(Characters * target, Characters * user);
};

