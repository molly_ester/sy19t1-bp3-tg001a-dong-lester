#pragma once
#include"Skill.h"
#include <iostream>

using namespace std;

class DopelBlade : public Skill {
public: 
	DopelBlade(int mp, string name);
	void Cast(Characters * target, Characters* user);
};