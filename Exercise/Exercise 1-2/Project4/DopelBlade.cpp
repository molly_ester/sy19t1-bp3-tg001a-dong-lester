#include "DopelBlade.h"
#include "Characters.h"

DopelBlade::DopelBlade(int mp, string name)
{
	this->mpCost = mp;
	this->name = name;
}

void DopelBlade::Cast(Characters * target, Characters * user)
{	
	if (user->getMp() >= this->mpCost) {
		user->modifyMp(this->mpCost);
		cout << user->getName() << "used up " << this->mpCost << " create a clone of itself and attacks the enemy\n";
		Characters *clone = new Characters(user->getName() + " -clone", user->getHp(), user->getMp(), user->getWeapon(), user->getSkill());
		clone->useSkill(target);
		delete clone;
	}
	else "not enough mp...";
}
