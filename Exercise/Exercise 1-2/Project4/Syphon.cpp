#include "Syphon.h"
#include "Characters.h"

Syphon::Syphon(int mp, string name)
{
	this->mpCost = mp;
	this->name = name;
}

void Syphon::Cast(Characters * target, Characters * user)
{
	if (user->getMp() >= this->mpCost) {
		user->modifyMp(this->mpCost);
		cout << user->getName() << " used " << this->mpCost << " mp drained hp from " << target->getName() << endl
			<< user->getName() << " dealt " << user->getWeapon()->getDamage() << " to " << target->getName() << endl
			<< user->getName() << " regained " << (int)(user->getWeapon()->getDamage()* 0.25f) << " hp.\n";
		target->takeDamage(user->getWeapon()->getDamage());
		user->takeDamage(-user->getWeapon()->getDamage() * 0.25f);
	}
	else cout << "not enough MP...";
}
