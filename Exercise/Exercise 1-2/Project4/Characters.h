#pragma once
#include<string>
#include <iostream>
#include<conio.h>
#include<vector>
#include "Weapons.h"

using namespace std;

class Skill;
class Characters
{
public:
	Characters(string name, int hp, int mp, Weapons * weapon, vector<Skill*> skill);
	~Characters();
	int getHp();
	int getMp();
	string getName();
	Weapons* getWeapon();
	void modifyMp(int value);
	vector<Skill*> getSkill();

	void takeDamage(int damage);
	void useSkill(Characters * target);
private:
	int hp, mp;
	string name;
	Weapons* weapon;
	vector<Skill*> skills;
};

