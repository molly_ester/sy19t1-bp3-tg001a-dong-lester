#include "Vengeance.h"
#include "Characters.h"
#include "Weapons.h"

Vengeance::Vengeance(int mp, string name)
{
	this->mpCost = mp;
	this->name = name;
}

void Vengeance::Cast(Characters * target, Characters *user)
{
	if (user->getMp() >= this->mpCost) {
		user->modifyMp(this->mpCost);
		cout << user->getName() << " used " << this->mpCost << " sacrificed hp to increase damage" << endl;
		user->takeDamage(user->getHp() * 0.25f);
		cout << user->getName() << " sacrificed " << (int)(user->getHp() * 0.25f) << " hp " << endl << user->getName() << " dealt " << user->getWeapon()->getDamage() * 2 << " to " << target->getName() << endl;
		if (user->getHp() <= 0) user->takeDamage(-1);
		target->takeDamage(user->getWeapon()->getDamage() * 2);
	}
	else cout << "not enough mp...";
}
