#pragma once
#include<iostream>
#include <string>
#include"Skill.h"

using namespace std;
class Attack : public Skill {
public:
	Attack(int mp, string name);
	void Cast(Characters * target, Characters * user);
};