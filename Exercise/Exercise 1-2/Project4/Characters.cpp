#include "Characters.h"
#include "Weapons.h"
#include "Skill.h"

Characters::Characters(string name, int hp, int mp, Weapons * weapon, vector<Skill*> skill)
{
	this->name = name;
	this->hp = hp;
	this->mp = mp;
	this->weapon = weapon;
	this->skills = skill;
}

Characters::~Characters()
{
}

int Characters::getHp()
{
	return this->hp;
}

int Characters::getMp()
{
	return this->mp;
}

string Characters::getName()
{
	return this->name;
}

Weapons* Characters::getWeapon()
{
	return this->weapon;
}

void Characters::modifyMp(int value) {
	this->mp -= value;
}

vector<Skill*> Characters::getSkill()
{
	return this->skills;
}

void Characters::takeDamage(int damage)
{
	this->hp -= damage;
}

void Characters::useSkill(Characters * target)
{
	int skillProcRate = rand()% 4;
	this->skills[skillProcRate]->Cast(target, this);
}

