#include<iostream>
#include<conio.h>
#include<string>
#include<time.h>

#include "Characters.h"
#include "Weapons.h"
#include "Skill.h"
#include "Attack.h"
#include "Syphon.h"
#include "Vengeance.h"
#include "DopelBlade.h"

using namespace std;

void displayUI(Characters* character1 , Characters* character2) {
	cout << "name: " << character1->getName() << "\nHP: " << character1->getHp() << "\nMP: " << character1->getMp() << "\nWeapon: " << character1->getWeapon()->getName() << endl << endl;
	cout << "name: " << character2->getName() << "\nHP: " << character2->getHp() << "\nMP: " << character2->getMp() << "\nWeapon: " << character2->getWeapon()->getName() << endl;
	cout << string(20, '=') << endl << endl;
}

void battle(Characters* c1, Characters* c2) {
	while (c1->getHp() > 0 && c2->getHp() > 0) {
		displayUI(c1, c2);
		c1->useSkill(c2);
		cout << endl;
		_getch();
		if (c2->getHp() <= 0) break;
		
		c2->useSkill(c1);
		_getch();
		system("cls");
	}
}

int main() {
	srand(time(NULL));
	Characters *character1 = new Characters("Annette", 350, 200, new Weapons(20, "Abraxas"), 
		{ new Attack(0, "Attack"), new Syphon(20, "Syphon"), new Vengeance(25, "Vengeance"), new DopelBlade(30, "Dopel Blade") });
	Characters *character2 = new Characters("Lysithea", 350, 200, new Weapons(20, "Luna"),
		{ new Attack(0, "Attack"), new Syphon(20, "Syphon"), new Vengeance(25, "Vengeance"), new DopelBlade(30, "Dopel Blade") });

	battle(character1, character2);

	if (character1->getHp() <= 0) cout << character1->getName() << " won the match!!!";
	else cout << character2->getName() << " won the match!!!";
	_getch();
	delete character1;
	delete character2;
}