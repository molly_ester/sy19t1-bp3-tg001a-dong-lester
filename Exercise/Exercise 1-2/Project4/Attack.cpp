#include "Attack.h"
#include "Characters.h"


Attack::Attack(int mp, string name)
{
	this->mpCost = mp;
	this->name = name;
}

void Attack::Cast(Characters * target, Characters * user)
{
	user->modifyMp(this->mpCost);
	int critChance = 20;
	int multiplier = 1;

	cout << user->getName() << " attacked " << target->getName() << " with " << user->getWeapon()->getName() << " ...";
	_getch();
	if (rand() % 100 + 1 <= 20) {
		multiplier = 2;
		cout << " it's a critical hit! ";
	}
	cout << user->getName() << " dealt " << user->getWeapon()->getDamage() * multiplier << " to " << target->getName() << endl;
	target->takeDamage(user->getWeapon()->getDamage() * multiplier);
}
