#pragma once
#include<string>
#include <iostream>
#include<conio.h>

using namespace std;

class Weapons;
class Characters 
{
public:
	Characters(string name, int hp, int mp, Weapons* weapon);
	~Characters();
	int getHp();
	int getMp();
	string getName();
	Weapons* getWeapon();
	void attack(Characters* target);
	
	void normalAttack(Characters * target);
	void criticalAttack(Characters * target);
	void syphon(Characters * target);
	void vengeance(Characters * target);
	void dopelBlade(Characters * target);
	void useSkill(Characters * target);
private:
	int hp, mp;
	string name;
	Weapons* weapon;
};

