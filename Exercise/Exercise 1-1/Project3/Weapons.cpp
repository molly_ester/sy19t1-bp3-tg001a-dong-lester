#include "Weapons.h"

Weapons::Weapons(int damage, string name)
{
	this->damage = damage;
	this->name = name;
}

int Weapons::getDamage()
{
	return this->damage;
}

string Weapons::getName()
{
	return this->name;
}

