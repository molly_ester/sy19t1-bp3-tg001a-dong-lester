#include "Characters.h"
#include "Weapons.h"

Characters::Characters(string name, int hp, int mp, Weapons * weapon)
{
	this->name = name;
	this->hp = hp;
	this->mp = mp;
	this->weapon = weapon;
}

Characters::~Characters()
{
	delete weapon;
}

int Characters::getHp()
{
	return this->hp;
}

int Characters::getMp()
{
	return this->mp;
}

string Characters::getName()
{
	return this->name;
}

Weapons* Characters::getWeapon()
{
	return this->weapon;
}

void Characters::attack(Characters *target)
{
	int critChance = 20;
	int multiplier = 1;
	
	cout << this->name << " attacked " << target->name << " with " << this->weapon->getName() << " ...";
	_getch();
	if (rand() % 100 + 1 <= 20) {
		multiplier = 2;
		cout << " it's a critical hit! ";
	}
	cout << this->name << " dealt " << this->weapon->getDamage() * multiplier << " to " << target->name << endl;
	target->hp -= this->weapon->getDamage() * multiplier;
}

void Characters::normalAttack(Characters * target)
{
	cout << " attack again and " << this->name << " dealt " << this->weapon->getDamage() << " to " << target->name << endl;
	target->hp -= this->weapon->getDamage();
}

void Characters::criticalAttack(Characters * target)
{
	cout << " make a fatal hit to " << target->name << " and " << this->name << " dealt " << this->weapon->getDamage() * 2 << " to " << target->name << endl;
	target->hp -= this->weapon->getDamage() * 2;
}

void Characters::syphon(Characters * target)
{
	cout << " drain hp from " << target->name << endl
		<< this->name << " dealt " << this->weapon->getDamage() << " to " << target->name << endl
		<< this->name << " regained " << (int) (this->weapon->getDamage()* 0.25f)<< " hp.\n";
	target->hp -= this->weapon->getDamage();
	this->hp += this->weapon->getDamage() * 0.25f;
}

void Characters::vengeance(Characters * target)
{
	cout << " sacrifice hp to increase damage" << endl;
	this->hp -= this->hp * 0.25f;
	cout << this->name << " sacrificed " << (int) (this->hp * 0.25f) << " hp " << endl << this->name << " dealt " << this->weapon->getDamage() * 2 << " to " << target->name << endl;
	if (this->hp <= 0) this->hp = 1;
	target->hp -= this->weapon->getDamage() * 2;
}

void Characters::dopelBlade(Characters * target)
{
	cout << " create a clone of itself and attacks the enemy\n";
	Characters *clone = new Characters(this->name + " -clone", this->hp, this->mp, this->weapon);
	clone->attack(target);
	clone->useSkill(target);
	delete clone;
}

void Characters::useSkill(Characters * target)
{
	int skillProcRate = rand() % 5 + 1;
	int mpCost = 20;
	if (this->mp >= mpCost) {
		cout << this->name << " used " << mpCost << " mp to...";
		if (skillProcRate == 1)this->normalAttack(target);
		else if (skillProcRate == 2) this->criticalAttack(target);
		else if (skillProcRate == 3) this->syphon(target);
		else if (skillProcRate == 4) this->vengeance(target);
		else this->dopelBlade(target);
	}
}

