#pragma once
#include<string>

using namespace std;

class Weapons
{
public:
	Weapons(int damage, string name);
	int getDamage();
	string getName();
private:
	int damage;
	string name;
};

