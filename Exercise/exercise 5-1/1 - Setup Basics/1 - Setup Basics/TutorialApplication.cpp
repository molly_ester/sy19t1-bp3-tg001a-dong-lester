/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h> 

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	spd = 2;
	acceleration = 0;
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	int size = 20;
	int plot = size / 2;
	ManualObject* obj = mSceneMgr->createManualObject("cube");
	obj->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	std::vector<Vector3> positions = {
		Vector3::Vector3(plot, plot, plot),
		Vector3::Vector3(-plot, -plot, plot),
		Vector3::Vector3(plot, -plot, plot),
		Vector3::Vector3(-plot, plot, plot),
		Vector3::Vector3(plot, plot, -plot),
		Vector3::Vector3(plot, -plot, -plot),
		Vector3::Vector3(-plot, plot, -plot),
		Vector3::Vector3(-plot, -plot, -plot),
	};

	for (auto& x : positions) {
		obj->position(x);
	}
	//front
	obj->index(0);
	obj->index(1);
	obj->index(2);

	obj->index(3);
	obj->index(1);
	obj->index(0);

	//left side
	obj->index(3);
	obj->index(7);
	obj->index(1);

	obj->index(6);
	obj->index(7);
	obj->index(3);

	//right side
	obj->index(4);
	obj->index(2);
	obj->index(5);

	obj->index(0);
	obj->index(2);
	obj->index(4);

	//top
	obj->index(4);
	obj->index(3);
	obj->index(0);

	obj->index(6);
	obj->index(3);
	obj->index(4);

	//bottom
	obj->index(1);
	obj->index(7);
	obj->index(5);

	obj->index(1);
	obj->index(5);
	obj->index(2);

	//back
	obj->index(5);
	obj->index(7);
	obj->index(4);

	obj->index(4);
	obj->index(7);
	obj->index(6);



	obj->end();

	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(obj);
}


bool TutorialApplication::frameStarted(const FrameEvent &evt)
{
	bool isPress = false;
	bool isX = false;
	bool isZ = false;
	if (mKeyboard->isKeyDown(OIS::KC_I)) {
		isZ = true;
		spd = 2;
		isPress = true;
	}
	if (mKeyboard->isKeyDown(OIS::KC_J)) {
		isX = true;
		spd = -2;
		isPress = true;
	}
	if (mKeyboard->isKeyDown(OIS::KC_K)) {
		isZ = true;
		spd = -2;
		isPress = true;
	}
	if (mKeyboard->isKeyDown(OIS::KC_L)) {
		isX = true;
		spd = 2;
		isPress = true;
	}

	if (isZ) {
			cubeNode->translate(0, 0, spd * acceleration);
	}
	if (isX) {
			cubeNode->translate(spd * acceleration, 0, 0);
	}

	if ((!isZ || !isX) && acceleration != 0) {
		isX = true;
		isZ = true;
	}
	


	if (!isPress) {
		if (acceleration > 0)
			acceleration -= evt.timeSinceLastFrame;
		if (acceleration < 0) acceleration = 0;
	}
	else {
		if (acceleration < 0.85f)
		acceleration += evt.timeSinceLastFrame;
	}

	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
