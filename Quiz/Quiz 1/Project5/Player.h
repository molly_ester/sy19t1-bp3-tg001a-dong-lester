#pragma once
#include<iostream>
#include<vector>
#include "Gacha.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "Bomb.h"
#include "Potion.h"
#include "Crystal.h"

using namespace std;
class Item;
class Player {
public:
	Player(int Hp, int Crystals);
	~Player();
	void pull(Gacha* pool);
	int GetRarityPoints();
	void setCrystals(int value);
	void SetHp(int value);
	void SetRarityPoints(int value);
	void displayStats();
	void displayResults();
	int GetHp();
	int GetCrystals();

	bool hasWon();
	bool canPull();
private:
	int hp;
	int crystals;
	int rarityPoints;
	vector<Item*> pulls[6];
	int counter;

};

