#pragma once
#include<iostream>
#include<string>
#include "Item.h"

using namespace std;
class Potion : public Item
{
public:
	Potion(int rates, string names, int value);
	~Potion();

	void effect(Player* player);
private:
	int healValue;
};

