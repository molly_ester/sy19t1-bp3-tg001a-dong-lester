#pragma once
#include <iostream>
#include<vector>
#include <iomanip>
#include<conio.h>

#include "Item.h"

using namespace std;
class Player;
class Gacha {
public:
	Gacha();
	~Gacha();
	Item* generateCard(int rarity);
	int weightedChance(int random);
	void displayRollnChance();
	Item* getItem(int i);

	void Game(Player* player);
private:
	vector<Item*> gacha;
	int itemChance[6];
};

