#pragma once
#include<iostream>
#include "Item.h"

using namespace std; 

class SSR : public Item {
public:
	SSR(int rates, string names, int rare);
	~SSR();

	void effect(Player * player);
private: 
	int rarityValue;
};

