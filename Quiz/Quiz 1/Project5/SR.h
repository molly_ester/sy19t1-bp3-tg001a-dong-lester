#pragma once
#include<iostream>
#include<string>
#include"Item.h"

using namespace std;

class SR : public Item { 
public:
	SR(int rates, string names, int rare);
	~SR();

	void effect(Player * player);
private:
	int rarityValue;
};

