#include "SSR.h"
#include "Player.h"

SSR::SSR(int rates, string names, int rare) : Item (rates, names)
{
	rarityValue = rare;
}

SSR::~SSR()
{
}

void SSR::effect(Player * player)
{
	cout << "You got an SSR card, gained " << rarityValue << " rarity points." << endl;
	player->SetRarityPoints(player->GetRarityPoints() + rarityValue);
}
