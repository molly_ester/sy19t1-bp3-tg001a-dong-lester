#pragma once
#include<iostream>
#include<string>
#include "Item.h"

using namespace std;

class Crystal : public Item {
public:
	Crystal(int rates, string names, int value);
	~Crystal();

	void effect(Player* player);
private:
	int crystalValue;
};

