#pragma once
#include<iostream>
#include<string>

using namespace std;

class Player;
class Item {
public:
	Item(int rates, string names);
	~Item();
	string GetName();
	int GetRate();
	virtual void effect(Player* player);
protected:
	int rate;
	string name;
};

