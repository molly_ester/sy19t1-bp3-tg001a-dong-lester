#include "Item.h"

Item::Item( int rates, string names) {
	rate = rates;
	name = names;
}

Item::~Item()
{
}

string Item::GetName()
{
	return name;
}

int Item::GetRate()
{
	return rate;
}

void Item::effect(Player * player)
{
}
