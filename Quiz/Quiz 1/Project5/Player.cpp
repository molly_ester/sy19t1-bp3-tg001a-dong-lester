#include "Player.h"
#include "Gacha.h"






Player::Player(int Hp, int Crystals)
{
	hp = Hp;
	crystals = Crystals;
	rarityPoints = 0;
	counter = 0;
}

Player::~Player()
{
	/*for (int i = 0; i < sizeof(pulls) / sizeof(pulls[0]); i++) {
		for (int k = 0; i < pulls[i].size(); k++)
			delete pulls[i][k];
		pulls[i].clear();
	}*/ //errors
	
	for (int i = 0; i < sizeof(pulls) / sizeof(pulls[0]); i++) {
		for (int k = 0; i < pulls[i].size(); k++)
			pulls[i].pop_back();
		pulls[i].clear();
	}
	
}

void Player::pull(Gacha * pool){
	crystals -= 5;
	counter++;
	int random = rand() % 100 + 1;
	int rarity = pool->weightedChance(random);
	pool->generateCard(rarity)->effect(this);
	pulls[rarity].push_back(pool->generateCard(rarity));
}

int Player::GetRarityPoints()
{
	return rarityPoints;
}

void Player::setCrystals(int value) {
	crystals = value;
}

void Player::SetHp(int value)
{
	hp = value;
	if (hp < 0) hp = 0;
}

void Player::SetRarityPoints(int value)
{
	rarityPoints = value;
}

void Player::displayStats()
{
	cout << "Hp: " << hp << endl << "Crystals: " << crystals << endl << "Rarity Points: " << rarityPoints << endl
		<< "Pulls: " << counter << endl << endl;
}

void Player::displayResults()
{
	cout << string(50, '=') << endl << endl;
	displayStats();
	cout << string(50, '=') << endl << endl;
	for (int i = 0; i < 6; i++) {
		if (pulls[i].size() > 0) cout << pulls[i][0]->GetName() << " x " << pulls[i].size() << endl;
	}
}

int Player::GetHp()
{
	return hp;
}

int Player::GetCrystals()
{
	return crystals;
}

bool Player::hasWon()
{
	return rarityPoints >= 100;
}

bool Player::canPull()
{
	return crystals >= 5 && hp > 0;
}
