#include "SR.h"
#include "Player.h"


SR::SR(int rates, string names, int rare) : Item (rates, names)
{
	rarityValue = rare;
}

SR::~SR()
{
}

void SR::effect(Player * player) {
	cout << "You got an SR card, gained " << rarityValue << " rarity points." << endl;
	player->SetRarityPoints(player->GetRarityPoints() + rarityValue);
}
