#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include "Player.h"

using namespace std;

int main() {
	srand(time(NULL));
	Player* player = new Player(100, 100);
	Gacha* gacha = new Gacha();

	gacha->Game(player);
	
	delete player;
	delete gacha;

	_getch();
	return 0;
}