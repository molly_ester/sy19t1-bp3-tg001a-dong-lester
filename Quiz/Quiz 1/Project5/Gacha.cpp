#include "Gacha.h"
#include "Player.h"



Gacha::Gacha()
{
	gacha = { new SSR(1, "SSR", 50), new SR(9, "SR", 10), new R(40, "R", 1), new Potion(15, "Health Potion", 30), 
		new Bomb(20, "Bomb", 25), new Crystal(15, "Crystals", 15) };
	
	for (int i = 0; i < 6; i++) {
		if (i != 0) itemChance[i] = gacha[i]->GetRate() + itemChance[i - 1];
		else itemChance[i] = gacha[i]->GetRate();
	}
}

Gacha::~Gacha()
{
	for (int i = 0; i < gacha.size(); i++) {
		gacha.pop_back();
	}
	gacha.clear();
}

Item * Gacha::generateCard(int rarity)
{
	return gacha[rarity];
}

int Gacha::weightedChance(int random) {
	for (int i = 0; i < sizeof(itemChance) / sizeof(itemChance[0]); i++)
		if (itemChance[i] >= random) return i;
}

void Gacha::displayRollnChance()
{
	cout << string(50, '=') << endl;
	for (int i = 0; i < 6; i++) {
		if (i == 0) cout << left << setw(20) << gacha[i]->GetName() << "\t\t\t" << itemChance[i] << "%";
		else cout << left << setw(20) << gacha[i]->GetName() << "\t\t\t" << itemChance[i] - itemChance[i - 1] << "%";
		cout << endl;
	}
	cout << endl;
}



Item * Gacha::getItem(int i)
{
	return gacha[i];
}

void Gacha::Game(Player * player)
{
	while (player->canPull() && !player->hasWon()) {
		player->displayStats();
		displayRollnChance();
		player->pull(this);

		_getch();
		system("Cls");
	}

	if (player->hasWon()) cout << "You Win... " << endl;
	else cout << "You Lost... " << endl;
	
	_getch();
	player->displayResults();

}
