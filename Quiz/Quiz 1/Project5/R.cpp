#include "R.h"
#include "Player.h"

R::R(int rates, string names, int rare) : Item (rates, names)
{
	rarityValue = rare;
}

R::~R()
{
}

void R::effect(Player * player) {
	cout << "You got an R card, gained " << rarityValue << " rarity point." << endl;
	player->SetRarityPoints(player->GetRarityPoints() + rarityValue);
}
