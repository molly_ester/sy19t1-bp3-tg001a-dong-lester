#include "Potion.h"
#include "Player.h"


Potion::Potion(int rates, string names, int value) : Item(rates, names)
{
	healValue = value;
}

Potion::~Potion()
{
}

void Potion::effect(Player * player)
{
	cout << "You got a potion... regained " << healValue << " health" << endl;
	player->SetHp(player->GetHp() + healValue);
}
