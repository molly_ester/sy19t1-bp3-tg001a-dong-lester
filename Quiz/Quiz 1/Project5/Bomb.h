#pragma once
#include<iostream>
#include<string>
#include "Item.h"

using namespace std;
class Bomb : public Item
{
public:
	Bomb(int rates, string names, int value);
	~Bomb();

	void effect(Player* player);
private:
	int damageValue;
};

