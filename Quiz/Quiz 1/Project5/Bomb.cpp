#include "Bomb.h"
#include "Player.h"

Bomb::Bomb(int rates, string names, int value) : Item(rates, names)
{
	damageValue = value;
}

Bomb::~Bomb()
{
}

void Bomb::effect(Player * player)
{
	cout << "You got a bomb... received " << damageValue << " damage" << endl;
	player->SetHp(player->GetHp() - damageValue);
}
