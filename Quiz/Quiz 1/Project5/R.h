#pragma once
#include<iostream>
#include<string>
#include "Item.h"
using namespace std;

class R : public Item {
public:
	R(int rates, string names, int rare);
	~R();

	void effect(Player* player);
protected: 
	int rarityValue;
};

