#include "Crystal.h"
#include "Player.h"



Crystal::Crystal(int rates, string names, int value) : Item(rates, names)
{
	crystalValue = value;
}

Crystal::~Crystal()
{
}

void Crystal::effect(Player* player)
{
	cout << "You got " << crystalValue << " crystals" << endl;
	player->setCrystals(player->GetCrystals() + crystalValue);
}
