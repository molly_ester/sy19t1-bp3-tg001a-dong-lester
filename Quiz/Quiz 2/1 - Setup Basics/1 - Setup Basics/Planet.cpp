#include "Planet.h"

Planet::~Planet()
{
}

void Planet::update(const FrameEvent & evt)
{
	float rotSpd = 24 / mLocalRotationSpeed;
	float revSpd = (365 / mRevolutionSpeed);
	Degree rot = Degree(60 * evt.timeSinceLastFrame * rotSpd);
	mNode->rotate(Vector3(0, 1, 0), Radian(rot));
	if ( mParent != NULL && mName != "moon") {
			Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * revSpd);
			Vector3 location = Vector3::ZERO;
			location.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
			location.z = mNode->getPosition().z - mParent->mNode->getPosition().z;
			float oldX = location.x;
			float oldZ = location.z;
			float newX = (oldX*Math::Cos(planetRevolution)) + (oldZ*Math::Sin(planetRevolution));
			float newZ = (oldX*-Math::Sin(planetRevolution)) + (oldZ*Math::Cos(planetRevolution));
			mNode->setPosition(newX, mNode->getPosition().y, newZ);
	}
	
}

SceneNode & Planet::getNode()
{
	return *mNode;
}

void Planet::setParent(Planet * parent)
{
	mParent = parent;
}

void Planet::setPosition(Vector3 vec) {
	mNode->setPosition(vec);
}

void Planet::attachMoon(Planet* moon) {
	moon->setParent(this);
	SceneNode* node = mNode->createChildSceneNode();
	node->attachObject(moon->mNode->detachObject(moon->mName));
	moon->mNode = NULL;
	moon->mNode = node;

}

Planet * Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}

Planet::Planet(SceneNode * node, std::string str){
	this->mNode = node;
	mName = str;
}

Planet* Planet::createPlanet(SceneManager& sceneManager, float halfSize, ColourValue colour, std::string str) {

	SceneNode* node = sceneManager.getRootSceneNode()->createChildSceneNode(str);
	ManualObject* object = sceneManager.createManualObject(str);
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	int triangle[12 * 3][3] = {
		{halfSize, halfSize, halfSize},
		{-halfSize, halfSize, halfSize},
		{-halfSize, -halfSize, halfSize},

		{halfSize, halfSize, halfSize},
		{-halfSize, -halfSize, halfSize},
		{halfSize, -halfSize, halfSize},

		{halfSize, halfSize, -halfSize},
		{halfSize, halfSize, halfSize},
		{halfSize, -halfSize, halfSize},

		{halfSize, halfSize, -halfSize},
		{halfSize, -halfSize, halfSize},
		{halfSize, -halfSize, -halfSize},

		{-halfSize, halfSize, -halfSize},
		{halfSize, halfSize, -halfSize},
		{halfSize, -halfSize, -halfSize},

		{-halfSize, halfSize, -halfSize},
		{halfSize, -halfSize, -halfSize},
		{-halfSize, -halfSize, -halfSize},

		{-halfSize, halfSize, halfSize},
		{-halfSize, halfSize, -halfSize},
		{-halfSize, -halfSize, -halfSize},

		{-halfSize, halfSize, halfSize},
		{-halfSize, -halfSize, -halfSize},
		{-halfSize, -halfSize, halfSize},

		{halfSize, halfSize, -halfSize},
		{-halfSize, halfSize, -halfSize},
		{-halfSize, halfSize, halfSize},

		{halfSize, halfSize, -halfSize},
		{-halfSize, halfSize, halfSize},
		{halfSize, halfSize, halfSize},

		{halfSize, -halfSize, halfSize},
		{-halfSize, -halfSize, halfSize},
		{-halfSize, -halfSize, -halfSize},

		{halfSize, -halfSize, halfSize},
		{-halfSize, -halfSize, -halfSize},
		{halfSize, -halfSize, -halfSize}
	};

	for (auto& i : triangle) {
		object->position(i[0], i[1], i[2]);
		object->colour(colour);
	}

	object->end();
	node->attachObject(object);

	return new Planet(node, str);
}