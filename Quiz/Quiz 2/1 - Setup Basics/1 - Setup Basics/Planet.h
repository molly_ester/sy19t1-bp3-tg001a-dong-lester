#pragma once
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>
using namespace Ogre;

class Planet 
{
public:
	Planet(SceneNode* node, std::string str);
	static Planet* createPlanet(SceneManager& sceneManager, float size, ColourValue colour, std::string str);
	~Planet();

	void update(const FrameEvent& evt);

	SceneNode& getNode();
	void setPosition(Vector3 vec);
	void setParent(Planet* parent);
	Planet* getParent();
	void attachMoon(Planet* moon);

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);
private:
	SceneNode* mNode;
	Planet* thisPlanet;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;
	std::string mName;
};