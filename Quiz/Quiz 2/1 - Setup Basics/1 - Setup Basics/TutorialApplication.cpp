/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>
#include <OgreLog.h>


//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	Vector3 distance[5] = { Vector3(57.91f, 0, 0), Vector3(108.2f, 0, 0), Vector3(149.6f, 0, 0), Vector3(227.9f, 0, 0), Vector3(38, 0, 0)};
	float spd[6] = { 24 ,1408, 5832, 24, 25, 1408 };
	float revolve[5] = { 88, 224, 365,687, 365};
	
	sun = Planet::createPlanet(*mSceneMgr, 10, ColourValue::ColourValue(1, 1, 0, 1), "sun");
	sun->setLocalRotationSpeed(spd[0]);
	sun->setPosition(Vector3::ZERO);

	planets.push_back(Planet::createPlanet(*mSceneMgr, 1.5f, ColourValue::ColourValue(0.59f, 0.29f, 0, 1), "mercury"));
	planets.push_back(Planet::createPlanet(*mSceneMgr, 2.5f, ColourValue::ColourValue(1, 0.075f, 0.8f, 1), "venus"));
	planets.push_back(Planet::createPlanet(*mSceneMgr, 5, ColourValue::Blue, "earth"));
	planets.push_back(Planet::createPlanet(*mSceneMgr, 4, ColourValue::Red, "mars"));

	for (Planet* planet : planets) {
		planet->setParent(sun);
		planet->setPosition(planets[i]->getParent()->getNode().getPosition() + distance[i]);
		planet->setLocalRotationSpeed(spd[i + 1]);
		planet->setRevolutionSpeed(revolve[i]);
	}

	moon = Planet::createPlanet(*mSceneMgr, 1, ColourValue::White, "moon");
	moon->setLocalRotationSpeed(spd[5]);
	moon->setRevolutionSpeed(revolve[4]);
	planets[2]->attachMoon(moon);
	moon->setPosition(distance[4]);

}

bool TutorialApplication::frameStarted(const Ogre::FrameEvent &evt)
{
	sun->update(evt);
	moon->update(evt);
	for (Planet* planet: planets){
		planet->update(evt);
	}
	

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
