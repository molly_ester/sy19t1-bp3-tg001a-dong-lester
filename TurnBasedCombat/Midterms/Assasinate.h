#pragma once
#include "Skill.h"
class Assasinate :
	public Skill
{
public:
	Assasinate();
	~Assasinate();

	void skillEffect(Unit* user, Team* target);
};

