#pragma once
#include<iostream>
#include<string>
#include <vector>
#include "Heal.h"
#include "Assasinate.h"
#include "Shockwave.h"

using namespace std;
class Unit;
class Skill;
class Job
{
public:
	Job();
	~Job();

	float getHpMultiplier();
	float getMpMultiplier();
	float getPowMultiplier();
	float getVitMultiplier();
	float getAgiMultiplier();
	float getDexMultiplier();
	string getName();
	float calculateMultiplier(Unit * target);
	Skill* getSkill();

protected:
	float hpMultiplier, mpMultiplier, powMultiplier, vitMultiplier, agiMultiplier, dexMultiplier;
	string name;
	string effective;
	Skill* skill;
};

