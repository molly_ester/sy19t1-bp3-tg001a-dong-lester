#pragma once
#include "Skill.h"
class Heal :
	public Skill
{
public:
	Heal();
	~Heal();

	void skillEffect(Unit* user, Team* target);
};

