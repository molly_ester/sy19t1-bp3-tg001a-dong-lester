#include "Assasin.h"
#include "Assasinate.h"



Assasin::Assasin()
{
	name = "Assassin";
	effective = "Mage";
	
	hpMultiplier = 0.9f;
	mpMultiplier = 0.9f;
	powMultiplier = 1.2f;
	vitMultiplier = 0.9f;
	agiMultiplier = 1.3f;
	dexMultiplier = 1.1f;

	skill = new Assasinate();
}


Assasin::~Assasin() 
{
	if (skill != NULL) delete skill;
}
