#include "PlayerUnit.h"

PlayerUnit::PlayerUnit(string name, Job * cJob, int Hp, int Mp, int Pow, int Vit, int Agi, int Dex) : Unit(name, cJob, Hp, Mp, Pow, Vit, Agi, Dex)
{

}

PlayerUnit::~PlayerUnit()
{
	delete job;
	delete attackSkill;
}

void PlayerUnit::unitTurn(Team * target, Team* allies)
{
	int input;
	displayStats();

	cout << "\t[" << 1 << "] " << getSkill()->getSkillName() << endl << "\t[" << 2 << "] " << getJob()->getSkill()->getSkillName() << endl;
	
	do {
		cout << "Input: ";
		cin >> input;
		if (input == 2 && getJob()->getSkill()->getMpCost() > mp) cout << "not enough mp." << endl;
	} while (input > 2 || input < 0 || (getJob()->getSkill()->getMpCost() > mp && input == 2));
	
	if (input == 1) {
		getSkill()->skillEffect(this, target);
	}
	else {
		if (getJob()->getSkill()->getSkillName() != "Heal") getJob()->getSkill()->skillEffect(this, target);
		else getJob()->getSkill()->skillEffect(this, allies);
	}
		
	_getch();
	system("Cls");
}

bool PlayerUnit::isPlayer()
{
	return true;
}
