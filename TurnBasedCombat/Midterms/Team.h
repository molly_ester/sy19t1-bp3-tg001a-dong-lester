#pragma once
#include<iostream>
#include<string>
#include <vector>
#include "Unit.h"
#include<iomanip>

using namespace std;
class Unit;
class Team
{
public:
	Team(vector<Unit*> Units, string name);
	~Team();

	void displayTeamStatus();
	bool isTeamAlive();
	Unit* getLowestHp();
	vector<Unit*> getUnits();
	string getName();
private:
	vector<Unit*> units;
	string name;
};

