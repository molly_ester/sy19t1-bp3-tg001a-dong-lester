#include "AiUnit.h"



AiUnit::AiUnit(string name, Job* cJob, int Hp, int Mp, int Pow, int Vit, int Agi, int Dex) : Unit(name, cJob, Hp, Mp, Pow, Vit, Agi, Dex)
{
}


AiUnit::~AiUnit()
{
	delete job;
	delete attackSkill;
}

void AiUnit::unitTurn(Team * target, Team* allies)
{
	int input = rand() % 2;
	displayStats();

	if (input == 0) getSkill()->skillEffect(this, target);
	else {
		if (getJob()->getSkill()->getMpCost() > mp) {
			cout << "not enough mp.";
			getSkill()->skillEffect(this, target);
		}
		else {
			if (getJob()->getSkill()->getSkillName() != "Heal") getJob()->getSkill()->skillEffect(this, target);
			else getJob()->getSkill()->skillEffect(this, allies);
		}
	}
	_getch();
	system("Cls");
}

bool AiUnit::isPlayer() {
	return false;
}
