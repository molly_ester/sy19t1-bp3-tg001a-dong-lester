#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <conio.h>
#include <assert.h>

using namespace std;
class Unit;
class Team;
class Skill
{
public:
	Skill();
	~Skill();
	int getMpCost();
	string getSkillName();
	int clamp(int value, int low, int high);
	void displayTargetableUnits(Team* target);
	Unit* chooseSingularTarget(Unit* user, Team* target);
	virtual void skillEffect(Unit* user, Team* target);
protected:
	int mpCost;
	float dmgCoefficient;
	string skillName;
	
};

