#include "Shockwave.h"
#include "Team.h"
#include "Unit.h"



Shockwave::Shockwave()
{
	mpCost = 25;
	dmgCoefficient = 0.9f;
	skillName = "Shockwave";
}


Shockwave::~Shockwave()
{

}

void Shockwave::skillEffect(Unit * user, Team * target)
{
	cout << user->getName() << " used " << mpCost << " mp to use Shockwave to deal damage to all enemies" << endl;
	user->setMp(user->getMp() - mpCost);
	for (int i = 0; i < target->getUnits().size(); i++) {
		if (target->getUnits()[i]->isAlive()) {
			float damage = ((user->randomedPow() * dmgCoefficient) - target->getUnits()[i]->getVit()) * user->getJob()->calculateMultiplier(target->getUnits()[i]);
			if (damage <= 0) damage = 1;
			cout << target->getUnits()[i]->getName() << " received " << (int) damage << " damage...";
			if (user->getJob()->calculateMultiplier(target->getUnits()[i]) > 1) cout << " it was effective." << endl;
			else cout << endl;
			
			target->getUnits()[i]->setHp(target->getUnits()[i]->getHp() - damage);
		}
	}
}
