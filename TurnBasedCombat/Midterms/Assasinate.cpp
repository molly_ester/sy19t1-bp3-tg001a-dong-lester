#include "Assasinate.h"
#include "Team.h"
#include "Unit.h"



Assasinate::Assasinate()
{
	mpCost = 25;
	dmgCoefficient = 2.2f;
	skillName = "Assasinate";
}


Assasinate::~Assasinate()
{
}

void Assasinate::skillEffect(Unit * user, Team * target)
{
	Unit* unitTarget;
	if (!user->isPlayer()) unitTarget = target->getLowestHp();
	else unitTarget = chooseSingularTarget(user, target);

	int hitChance = clamp((user->getDex() / unitTarget->getAgi()) * 100, 20, 80);
	float damage = ((user->randomedPow() * dmgCoefficient) - unitTarget->getVit()) * user->getJob()->calculateMultiplier(unitTarget);
	user->setMp(mpCost);

	cout << user->getName() << " used " << mpCost << " mp to assassinate " << unitTarget->getName() << endl
		<< user->getName() << "'s " << skillName << " dealt " << (int) damage << " to " << unitTarget->getName() << "...";
	if (user->getJob()->calculateMultiplier(unitTarget) > 1) cout << "it was effective." << endl;
	unitTarget->setHp(unitTarget->getHp() - damage);
}
