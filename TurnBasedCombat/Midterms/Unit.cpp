#include "Unit.h"
#include "Attack.h"

Unit::Unit(string Name, Job * cJob, int Hp, int Mp, int Pow, int Vit, int Agi, int Dex)
{
	job = cJob;
	name = Name;
	maxHp = Hp * job->getHpMultiplier();
	hp = maxHp;
	mp = Mp * job->getMpMultiplier();
	pow = Pow * job->getPowMultiplier();
	vit = Vit * job->getVitMultiplier();
	agi = Agi * job->getAgiMultiplier();
	dex = Dex * job->getDexMultiplier();

	attackSkill = new Attack();
}

Unit::~Unit()
{
	delete job;
	delete attackSkill;
}

int Unit::getMaxHp()
{
	return maxHp;
}

int Unit::getHp()
{
	return hp;
}

int Unit::getMp()
{
	return mp;
}

int Unit::getPow()
{
	return pow;
}

int Unit::getVit()
{
	return vit;
}

int Unit::getAgi()
{
	return agi;
}

int Unit::getDex()
{
	return dex;
}

string Unit::getName()
{
	return name;
}

bool Unit::isAlive()
{
	return hp > 0;
}

int Unit::randomedPow()
{
	return pow + (pow * 0.2f);
}

void Unit::setHp(int value)
{
	hp = value;
	if (hp < 0) hp = 0;
	if (hp > maxHp) hp = maxHp;
}

void Unit::setMp(int value)
{
	mp = value;
	if (mp < 0) mp = 0;
}

void Unit::displayStats()
{
	cout << string(30, '=') << endl << "Name: " << name << endl
		<< "HP: " << hp << endl
		<< "MP: " << mp << endl
		<< "Pow: " << pow << endl
		<< "Vit: " << vit << endl
		<< "Agi: " << agi << endl
		<< "Dex: " << dex << endl << string(30, '=') << endl << endl;
}

Job * Unit::getJob()
{
	return job;
}

void Unit::unitTurn(Team * target, Team* allies)
{
}

Skill * Unit::getSkill()
{
	return attackSkill;
}

bool Unit::isPlayer() {
	return 0;
}
