#include "Heal.h"
#include "Team.h"
#include "Unit.h"

Heal::Heal()
{
	mpCost = 15;
	dmgCoefficient = 0.0f;
	skillName = "Heal";
}


Heal::~Heal()
{
}

void Heal::skillEffect(Unit * user, Team * target)
{
	Unit* unitToHeal;
	if (!user->isPlayer()) unitToHeal = target->getLowestHp();
	else unitToHeal = chooseSingularTarget(user, target);
	
	cout << user->getName() << " uses " << mpCost << " mp to heal " << unitToHeal->getName() << " with " << (int) (unitToHeal->getMaxHp() * 0.3f) << " Hp" << endl;
	user->setMp(user->getMp() - mpCost);
	unitToHeal->setHp(unitToHeal->getHp() + (unitToHeal->getMaxHp() * 0.3f));
}
