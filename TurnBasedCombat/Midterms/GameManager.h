#pragma once
#include "Team.h"
class GameManager
{
public:
	GameManager(vector<Team*> team);
	~GameManager();

	void sortSpeedOrder();
	void displayOrder();
	void changeOrder();
	void checkForDead();
	void Game();
	Team * identifyTeam();
	Team* identifyEnemyTeam();



private:
	vector<Team*> teams;
	vector<Unit*> speedOrder;
};

