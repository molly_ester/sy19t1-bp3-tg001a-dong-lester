#include "GameManager.h"




GameManager::GameManager(vector<Team*> team)
{
	teams = team;

	for (int i = 0; i < team.size(); i++) {
		for (int k = 0; k < team[i]->getUnits().size(); k++) {
			speedOrder.push_back(teams[i]->getUnits()[k]);
		}
	}
}

GameManager::~GameManager()
{
	speedOrder.clear();
	for (int i = 0; i < teams.size(); i++) delete teams[i];
	teams.clear();
}

void GameManager::sortSpeedOrder()
{
	for (int i = 0; i < speedOrder.size(); i++) {
		int highest = i;
		for (int j = i + 1; j < speedOrder.size(); j++) {
			if (speedOrder[j]->getAgi() > speedOrder[highest]->getAgi()) highest = j;
		}
		swap(speedOrder[i], speedOrder[highest]);
	}
}

void GameManager::displayOrder()
{
	cout << string(13, '=') << "Turns" << string(12, '=') << endl;
	for (int i = 0; i < speedOrder.size(); i++) 
		cout << left << setw(15) << speedOrder[i]->getName() << "\tAgi: " << speedOrder[i]->getAgi() << endl;
	cout << string(30, '=') << endl;
}

void GameManager::changeOrder()
{
	speedOrder.push_back(speedOrder[0]);
	speedOrder.erase(speedOrder.begin());
}

void GameManager::checkForDead()
{
	for (int i = 0; i < speedOrder.size(); i++)
		if (!speedOrder[i]->isAlive()) speedOrder.erase(speedOrder.begin() + i);
}

void GameManager::Game()
{
	cout << R"(
______ _            _____          _     _                        
|  ___(_)          |  ___|        | |   | |               _       
| |_   _ _ __ ___  | |__ _ __ ___ | |__ | | ___ _ __ ___ (_)      
|  _| | | '__/ _ \ |  __| '_ ` _ \| '_ \| |/ _ \ '_ ` _ \         
| |   | | | |  __/ | |__| | | | | | |_) | |  __/ | | | | |_       
\_|   |_|_|  \___| \____/_| |_| |_|_.__/|_|\___|_| |_| |_(_)      
                                                                  
                                                                  
      _____ _                     _   _                           
     |_   _| |                   | | | |                          
       | | | |__  _ __ ___  ___  | |_| | ___  _   _ ___  ___  ___ 
       | | | '_ \| '__/ _ \/ _ \ |  _  |/ _ \| | | / __|/ _ \/ __|
       | | | | | | | |  __/  __/ | | | | (_) | |_| \__ \  __/\__ \
       \_/ |_| |_|_|  \___|\___| \_| |_/\___/ \__,_|___/\___||___/
                                                                  
                                                                  
)";
	_getch();
	system("CLS");
	sortSpeedOrder();
	while (teams[0]->isTeamAlive() && teams[1]->isTeamAlive()) {
		for (int i = 0; i < 2; i++) teams[i]->displayTeamStatus();
		displayOrder();
		_getch();
		system("Cls");

		speedOrder[0]->unitTurn(identifyEnemyTeam(), identifyTeam());
		system("cls");
		checkForDead();
		changeOrder();
	}
	
	for (int i = 0; i < 2; i++) 
		if (teams[i]->isTeamAlive()) {
			cout << teams[i]->getName() << " won!!";
			break;
		}
	
	_getch();
}

Team * GameManager::identifyTeam()
{
	for (int i = 0; i < teams.size(); i++) {
 		for (int j = 0; j < teams[i]->getUnits().size(); j++) {
			if (speedOrder[0]->getName() == teams[i]->getUnits()[j]->getName()) return teams[i];
		}
	}
}

Team * GameManager::identifyEnemyTeam()
{
	for (int i = 0; i < teams.size(); i++) {
		for (int j = 0; j < teams[i]->getUnits().size(); j++) {
			if (speedOrder[0]->getName() == teams[i]->getUnits()[j]->getName()) return teams[(i + 1) % 2];
		}
	}
}
