#include "Team.h"
#include "PlayerUnit.h"
#include "AiUnit.h"

Team::Team(vector<Unit*> Units, string Name)
{
	units = Units;
	name = Name;
}

Team::~Team()
{
	for (int i = 0; i < units.size(); i++) delete units[i];
	units.clear();
}

void Team::displayTeamStatus()
{
	cout << string(30, '=') << endl;
	cout << name << ": " << endl;
	for (int i = 0; i < units.size(); i++) { 
		cout << left << setw(10) << units[i]->getName() << ":\tHP: "; 
		if (!units[i]->isAlive()) cout << "[Dead]\n";
		else cout << "[" << units[i]->getHp() << "/" << units[i]->getMaxHp()<< "]\n";
	}
	cout << endl;
}

bool Team::isTeamAlive()
{
	return units[0]->isAlive() || units[1]->isAlive() || units[2]->isAlive();
}

Unit * Team::getLowestHp()
{
	
	int lowest = INT_MAX;
	int place;
	for (int i = 0; i < units.size(); i++) {
		if (units[i]->isAlive()) {
			if (units[i]->getHp() < lowest) {
				lowest = units[i]->getHp();
				place = i;
			}
		}
	}
	return units[place];
}

vector<Unit*> Team::getUnits()
{
	return units;
}

string Team::getName()
{
	return name;
}
