#include "Attack.h"
#include "Team.h"
#include "Unit.h"



Attack::Attack()
{
	mpCost = 0;
	dmgCoefficient = 1;
	skillName = "Attack";
}


Attack::~Attack()
{
}

void Attack::skillEffect(Unit * user, Team * target)
{
	user->setMp(user->getMp() - mpCost);
	Unit* unitTarget;
	if (!user->isPlayer()) do unitTarget = target->getUnits()[rand() % target->getUnits().size()]; while (!unitTarget->isAlive());
	else  unitTarget = chooseSingularTarget(user, target);


	int hitChance = clamp((user->getDex() / unitTarget->getAgi()) * 100, 20, 80);
	float critMultiplier = 1.0f;
	float damage = ((user->randomedPow() * dmgCoefficient) - unitTarget->getVit()) * user->getJob()->calculateMultiplier(unitTarget);
	if (damage <= 0) damage = 1;

	if (rand() % 100 + 1 <= hitChance) {
		cout << user->getName() << " attacks " << unitTarget->getName() << "... ";
		if (rand() % 100 + 1 <= 20) {
			cout << "it's a critical hit.\n";
			critMultiplier = 1.2f;
		}
		cout << unitTarget->getName() << " receives " << (int) damage << " damage...";
		if (user->getJob()->calculateMultiplier(unitTarget) > 1) cout << "it was effective." << endl;
		unitTarget->setHp(unitTarget->getHp() - (damage * critMultiplier));
	}
	else cout << user->getName() << " misses its attack." << endl;
}
