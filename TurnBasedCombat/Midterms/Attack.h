#pragma once
#include "Skill.h"
class Attack :
	public Skill
{
public:
	Attack();
	~Attack();

	void skillEffect(Unit* user, Team* target);
};

