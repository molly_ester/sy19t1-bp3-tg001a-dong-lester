#pragma once
#include "Skill.h"
class Shockwave :
	public Skill
{
public:
	Shockwave();
	~Shockwave();

	void skillEffect(Unit* user, Team* target);
};

