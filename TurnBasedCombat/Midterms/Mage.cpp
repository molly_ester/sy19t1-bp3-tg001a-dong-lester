#include "Mage.h"
#include "Heal.h"



Mage::Mage()
{
	name = "Mage";
	effective = "Warrior";

	hpMultiplier = 0.8f;
	mpMultiplier = 1.5f;
	powMultiplier = 0.9f;
	vitMultiplier = 0.8f;
	agiMultiplier = 1.2f;
	dexMultiplier = 1.2f;

	skill = new Heal();
}


Mage::~Mage()
{
	if (skill != NULL) delete skill;
}
