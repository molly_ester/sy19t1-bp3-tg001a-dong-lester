#pragma once
#include "Unit.h"
class PlayerUnit : public Unit
{
public:
	PlayerUnit(string name, Job* cJob, int Hp, int Mp, int Pow, int Vit, int Agi, int Dex);
	~PlayerUnit();

	void unitTurn(Team* target, Team* Allies);
	bool isPlayer();
};

