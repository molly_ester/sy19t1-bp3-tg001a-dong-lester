#pragma once
#include "Unit.h"
class AiUnit : public Unit
{
public:
	AiUnit(string name, Job* cJob, int Hp, int Mp, int Pow, int Vit, int Agi, int Dex);
	~AiUnit();

	void unitTurn(Team* target, Team* allies);
	bool isPlayer();
};

