#include "Job.h"
#include "Unit.h"



Job::Job()
{
}


Job::~Job()
{
	if (skill != NULL) delete skill;
}

float Job::getHpMultiplier()
{
	return hpMultiplier;
}

float Job::getMpMultiplier()
{
	return mpMultiplier;
}

float Job::getPowMultiplier()
{
	return powMultiplier;
}

float Job::getVitMultiplier()
{
	return vitMultiplier;
}

float Job::getAgiMultiplier()
{
	return agiMultiplier;
}

float Job::getDexMultiplier()
{
	return dexMultiplier;
}

string Job::getName()
{
	return name;
}

float Job::calculateMultiplier(Unit * target)
{
	if (target->getJob()->name == effective) return 1.5f;
	else return 1;
}

Skill * Job::getSkill()
{
	return skill;
}
