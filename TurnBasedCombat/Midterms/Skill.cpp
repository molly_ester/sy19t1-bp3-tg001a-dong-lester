#include "Skill.h"
#include "Team.h"
#include <iomanip>



Skill::Skill()
{
}


Skill::~Skill()
{
}

int Skill::getMpCost()
{
	return mpCost;
}

string Skill::getSkillName()
{
	return skillName;
}

int Skill::clamp(int value, int low, int high)
{
	if (value > high) return high;
	else if (value < low) return low;
	else return value;
}

void Skill::skillEffect(Unit * user, Team * target)
{
}

void Skill::displayTargetableUnits(Team* target) {
	for (int i = 0; i < target->getUnits().size(); i++) {
		cout << left << "[" << i + 1 << "]\t" << setw(10) << target->getUnits()[i]->getName() << "\tHP: ";
		if (!target->getUnits()[i]->isAlive()) cout << "[Dead]\n";
		else cout << "[" << target->getUnits()[i]->getHp() << "/" << target->getUnits()[i]->getMaxHp() << "]\n";
	}
}

Unit* Skill::chooseSingularTarget(Unit* user, Team*target) {
	assert(user->isPlayer());
	int input;
	displayTargetableUnits(target);
	cout << "Dead units cannot be healed/attacked\n\n";
	do {
		cout << "Input: ";
		cin >> input;
		if (input > target->getUnits().size() || input < 1 ) cout << "invalid Input\n";
		else if (!target->getUnits()[input - 1]->isAlive()) cout << target->getUnits()[input - 1]->getName() << " is dead, choose other units\n";
	} while (input > target->getUnits().size() || input < 1 || !target->getUnits()[input - 1]->isAlive());
	
	return target->getUnits()[input - 1];
}
