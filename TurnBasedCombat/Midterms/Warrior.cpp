#include "Warrior.h"
#include "Shockwave.h"



Warrior::Warrior()
{
	name = "Warrior";
	effective = "Assasin";

	hpMultiplier = 1.4f;
	mpMultiplier = 0.9f;
	powMultiplier = 1.1f;
	vitMultiplier = 1.2f;
	agiMultiplier = 0.8f;
	dexMultiplier = 0.9f;

	skill = new Shockwave();
}


Warrior::~Warrior()
{
	if (skill != NULL) delete skill;
}
