#pragma once
#include<iostream>
#include<string>
#include <vector>
#include "Job.h"
#include "Skill.h"
#include "Team.h"

using namespace std;
class Unit
{
public:
	Unit(string Name, Job* cJob, int Hp, int Mp, int Pow, int Vit, int Agi, int Dex);
	~Unit();

	int getMaxHp();
	int getHp();
	int getMp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	string getName();
	bool isAlive();
	int randomedPow();

	void setHp(int value);
	void setMp(int value);
	void displayStats();

	Job* getJob();
	virtual void unitTurn(Team* target, Team* allies);
	virtual bool isPlayer();
	Skill* getSkill();
protected:
	int hp, maxHp, pow, vit, agi, dex, mp;
	string name;
	Job* job;
	Skill* attackSkill;

};

