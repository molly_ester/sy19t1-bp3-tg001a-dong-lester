#include<iostream>
#include <vector>
#include "GameManager.h"
#include "Unit.h"
#include "PlayerUnit.h"
//#include <time.h>
#include "AiUnit.h"
#include "Mage.h"
#include "Assasin.h"
#include "Warrior.h"


int random(int start, int end) {
	return rand() % (end - start + 1) + start;
}


int main() {
	srand(time(NULL));
	/*Team* blueLions = new Team({new PlayerUnit("Annette", new Mage(), random(10, 20) * 5, random(10,20) *5,random(50 ,60), random(30, 40),random(30 ,40),random(40 ,70)),
		new PlayerUnit("Dimitri", new Warrior(), random(10,20) * 5, random(10,20) * 5, random(50 ,60),random(30, 40),random(30 ,40), random(40 ,70)),
		new PlayerUnit("Ashe", new Assasin(), random(10,20) *5, random(10,20)  *5, random(50 ,60), random(30, 40),random(30 ,40) ,random(40 ,70))}, "Blue Lions");
	Team* blackEagles = new Team({ new AiUnit("Dorothea", new Mage(), random(10, 20) * 5, random(10,20) * 5, random(50 ,60),random(30, 40),random(30 ,40), random(40 ,70)),
		new AiUnit("Edelgard", new Warrior(), random(10,20) * 5, random(10,20) * 5,random(50 ,60), random(30, 40),random(30 ,40), random(40 ,70)),
		new AiUnit("Petra", new Assasin(), random(10,20) * 5, random(10,20) * 5,random(50 ,60), random(30, 40),random(30 ,40) ,random(40 ,70))}, "Black Eagles");*/
	GameManager* game = new GameManager({ 
		new Team({new PlayerUnit("Annette", new Mage(), random(10, 20) * 5, random(10,20) * 5,random(50 ,60), random(30, 40),random(30 ,40),random(40 ,70)),
		new PlayerUnit("Dimitri", new Warrior(), random(10,20) * 5, random(10,20) * 5, random(50 ,60),random(30, 40),random(30 ,40), random(40 ,70)),
		new PlayerUnit("Ashe", new Assasin(), random(10,20) * 5, random(10,20) * 5, random(50 ,60), random(30, 40),random(30 ,40) ,random(40 ,70))}, "Blue Lions"),
		new Team({ new AiUnit("Dorothea", new Mage(), random(10, 20) * 5, random(10,20) * 5, random(50 ,60),random(30, 40),random(30 ,40), random(40 ,70)),
		new AiUnit("Edelgard", new Warrior(), random(10,20) * 5, random(10,20) * 5,random(50 ,60), random(30, 40),random(30 ,40), random(40 ,70)),
		new AiUnit("Petra", new Assasin(), random(10,20) * 5, random(10,20) * 5,random(50 ,60), random(30, 40),random(30 ,40) ,random(40 ,70))}, "Black Eagles") });

	game->Game();

	delete game;
	
}