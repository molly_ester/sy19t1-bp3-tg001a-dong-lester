#include "Moves.h"
#include "Types.h"
#include "Pokemon.h"



Moves::Moves(string name, Types* type, int damage) {
	this->name = name;
	this->type = type;
	this->damage = damage;
}

void Moves::computeMultiplier(Pokemon* pokemon){
	this->multiplier = 1.0f;
	bool isEffective = false;
	bool isIneffective = false;

	for (int i = 0; i < sizeof(*pokemon->type) / sizeof(pokemon->type[0]); i++) { //pokemon type
		for (int k = 0; k < pokemon->type[i]->strength.size(); k++) { //pokemon weaknesses
			if (pokemon->type[i]->strength[k] == this->type->name) {
				this->multiplier *= 0.5f;
				isIneffective = true;
			}
		}
	}

	if(isIneffective == false){
		for (int i = 0; i < sizeof(*pokemon->type) / sizeof(pokemon->type[0]); i++) { //pokemon type
			for (int k = 0; k < pokemon->type[i]->weaknesses.size(); k++) { //pokemon weaknesses
				if (pokemon->type[i]->weaknesses[k] == this->type->name) {
					this->multiplier *= 2;
					isEffective == true;
				}
			}
		
		}
	}
	
	 if (isEffective == false) {
		for (int i = 0; i < sizeof(*pokemon->type) / sizeof(pokemon->type[0]); i++) {
			for (int j = 0; j < pokemon->type[i]->immune.size(); j++) {
				if (this->type->name == pokemon->type[i]->immune[j]) 
					this->multiplier = 0;
			}
		}
	 }
}

void Moves::applyDamage(Pokemon* attacker, Pokemon* enemy)
{
	this->computeMultiplier(enemy);
	int dodgeRate = 20;
	cout << attacker->name << " used " << attacker->move->name << "... ";
	if (dodgeRate < rand() % 100 + 1) {
		float damage = (((((2 * attacker->lvl) / 5) + 2) * this->damage * (attacker->atk / enemy->def) / 50) + 2) * multiplier;
		enemy->hp -= ceil(damage);
		if (enemy->hp < 0) enemy->hp = 0;
		if (multiplier >= 2) cout << " it was very effective... ";
		else if (multiplier < 1) cout << " it's not very effective... ";
		else if (multiplier == 0) cout << " but it had no effect... ";
		cout << attacker->name << " dealt " << damage << " to " << enemy->name << endl;
	}
	else cout << "it missed...\n";
}


