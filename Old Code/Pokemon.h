#pragma once
#include <iostream>
#include <string>
#include<conio.h>

using namespace std;
class Types;
class Moves;
class Pokemon
{
private: 
	int exp, maxExp;
public:
	Pokemon(string name, int baseHp, int atk, int def, int lvl);
	Pokemon(string name, int baseHp, int atk, int def, int lvl, Types* type1, Types* type2, Moves* move);
	Pokemon(string name, Types* type1, Types* type2, Moves* move);
	Pokemon(string name);
	string name;
	Moves* move;
	Types* type[2];
	int baseHp, hp, atk, def, lvl, maxHp;
	Pokemon* evolve = NULL;
	int evolveLvl = NULL;
	

	void lvlUp();
	void cleanTypes();
	Pokemon* evolution();
	void gainExp();
	void showStats();

	void showWeaknesses();
	
};

