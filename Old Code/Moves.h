#pragma once
#include <iostream>
#include <string>
#include <cmath>

using namespace std;
class Types;
class Pokemon;
class Moves
{
public:
	Moves(string name, Types* type, int damage);
	string name;
	Types* type;
	int damage;
	double multiplier;

	void computeMultiplier(Pokemon* pokemon);
	void applyDamage(Pokemon * attacker, Pokemon * enemy);

};

