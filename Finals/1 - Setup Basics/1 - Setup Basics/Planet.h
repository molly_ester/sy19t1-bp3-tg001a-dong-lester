#pragma once
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>
using namespace Ogre;

class Planet 
{
public:
	Planet(SceneNode* node, std::string str);
	static Planet* createPlanet(SceneManager& sceneMgr, ManualObject* obj);
	~Planet();

	void update(const FrameEvent& evt);

	SceneNode& getNode();
	void setPosition(Vector3 vec);
	void setParent(Planet* parent);
	Planet* getParent();
	void attachMoon(Planet* moon);
	void rotation(const FrameEvent& evt);
	void revolution(const FrameEvent& evt);
	Vector3 getPosition();
	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);
private:
	SceneNode* mNode;
	Planet* thisPlanet;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;
	
	
	std::string mName;
};