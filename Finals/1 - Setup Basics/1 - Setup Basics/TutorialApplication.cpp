﻿/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/
#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	//mCamera->setPolygonMode(PolygonMode::PM_WIREFRAME);
	
	Vector3 distance[5] = { Vector3(57.91f, 0, 0), Vector3(108.2f, 0, 0), Vector3(149.6f, 0, 0), Vector3(227.9f, 0, 0), Vector3(38, 0, 0) };
	float spd[6] = { 24 ,1408, 5832, 24, 25, 1408 };
	float revolve[5] = { 88, 224, 365,687, 365 };
	
	sun = Planet::createPlanet(*mSceneMgr, createProceduralObject(20, ColourValue::ColourValue(1, 1, 0), "sun"));
	sun->setLocalRotationSpeed(spd[0]);
	sun->setPosition(Vector3::ZERO);

	planets.push_back(Planet::createPlanet(*mSceneMgr, createProceduralObject(3, ColourValue(0.59f, 0.29f, 0, 1), "mercury")));
	planets.push_back(Planet::createPlanet(*mSceneMgr, createProceduralObject(5, ColourValue(0.82f, 0.7f, 0.54f), "venus")));
	planets.push_back(Planet::createPlanet(*mSceneMgr, createProceduralObject(10, ColourValue::Blue, "earth")));
	planets.push_back(Planet::createPlanet(*mSceneMgr, createProceduralObject(8, ColourValue(0.71f, 0.25f, 0.05f), "mars")));
	

	for (int i = 0; i < planets.size(); i++) {
		planets[i]->setParent(sun);
		planets[i]->setPosition(planets[i]->getParent()->getNode().getPosition() + distance[i]);
		planets[i]->setLocalRotationSpeed(spd[i + 1]);
		planets[i]->setRevolutionSpeed(revolve[i]);
	}

	moon = Planet::createPlanet(*mSceneMgr, createProceduralObject(1, ColourValue(0.7f, 0.7f, 0.7f), "moon"));
	moon->setLocalRotationSpeed(spd[5]);
	moon->setRevolutionSpeed(revolve[4]);
	planets[2]->attachMoon(moon);
	moon->setPosition(distance[4]);

	// Lecture: Adding a light source
	Light *pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 1.0f));
	pointLight->setSpecularColour(ColourValue(0.3f, 0.3f, 0.3f));
	// Values taken from: http://www.ogre3d.org/tikiwiki/-Point+Light+Attenuation
	//pointLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
	pointLight->setCastShadows(false);

	// Lecture: Setting ambient light
	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	sun->update(evt);
	moon->update(evt);
	for (Planet* x : planets) {
		x->update(evt);
	}
	
	return 1;
}

ManualObject * TutorialApplication::createProceduralObject(float size, ColourValue colour, std::string str)
{
	ManualObject* manual = mSceneMgr->createManualObject(str);

	// NOTE: The second parameter to the create method is the resource group the material will be added to.
	// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(str, "General");
	myManualObjectMaterial->setReceiveShadows(false);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);

	if (str == "sun") manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	else manual->begin(str, RenderOperation::OT_TRIANGLE_LIST);

	// Create a circle by using rotation formula to find the points
	// And 0,0 as center

	// Store the vertices in a vector first (needed later)
	std::vector<Vector3> vertices;

	// Compute for the vertices first

	int NUM_POINTS = 360;
	float height = NUM_POINTS / 2;
	float fDeltaRingAngle = (Math::PI / height);
	float fDeltaSegAngle = (2 * Math::PI / NUM_POINTS);
	float radius = size / 2;
	//formula from http://wiki.ogre3d.org/ManualSphereMeshes
	for (int j = 0; j <= height; j++) {
		float r0 = radius * sinf(j * fDeltaRingAngle);
		float y0 = radius * cosf(j * fDeltaRingAngle);
		for (int i = 0; i <= NUM_POINTS; i++)
		{
			float x0 = r0 * sinf(i * fDeltaSegAngle);
			float z0 = r0 * cosf(i * fDeltaSegAngle);
			Vector3 vert(x0, y0, z0);
			vertices.push_back(vert);
		}
	}

	// Plot vertices
	for (int i = 0; i < vertices.size(); i++)
	{
		manual->position(vertices[i]);
		manual->colour(colour);
		//Compute for normals
		manual->normal(vertices[i].normalisedCopy());
	}


	for (int i = 1; i <= (NUM_POINTS + 1) * (height)-1; i++) {
		manual->index(i);
		manual->index((i + NUM_POINTS));
		manual->index((i + NUM_POINTS + 1));

		manual->index((i + 1));
		manual->index(i);
		manual->index((i + NUM_POINTS + 1));
	}

	/*manual->index((NUM_POINTS + 1) * (height + 1));
	manual->index((NUM_POINTS + 1) * (height + 1) -	 1);
	manual->index((NUM_POINTS + 1) * (height));*/

	manual->end();
	return manual;
}


//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
