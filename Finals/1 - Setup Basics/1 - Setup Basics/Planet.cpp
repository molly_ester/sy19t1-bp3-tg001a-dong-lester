#include "Planet.h"


Planet::~Planet()
{
}

void Planet::update(const FrameEvent & evt)
{
	rotation(evt);
	if (mParent != NULL && mName != "moon")
		revolution(evt);
	
}

SceneNode & Planet::getNode()
{
	return *mNode;
}

void Planet::setParent(Planet * parent)
{
	mParent = parent;
}

void Planet::setPosition(Vector3 vec) {
	mNode->setPosition(vec);
}

void Planet::attachMoon(Planet* moon) {
	moon->setParent(this);
	SceneNode* node = mNode->createChildSceneNode();
	node->attachObject(moon->mNode->detachObject(moon->mName));
	moon->mNode = NULL;
	moon->mNode = node;

}

void Planet::rotation(const FrameEvent & evt)
{
	float rotSpd = 24 / mLocalRotationSpeed;
	Degree rot = Degree(evt.timeSinceLastFrame * rotSpd);
	mNode->rotate(Vector3(0, 1, 0), Radian(rot));
}

void Planet::revolution(const FrameEvent & evt)
{
	float revSpd = (365 / mRevolutionSpeed);
	Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * revSpd);
	Vector3 location = Vector3::ZERO;
	location.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
	location.z = mNode->getPosition().z - mParent->mNode->getPosition().z;
	float oldX = location.x;
	float oldZ = location.z;
	float newX = (oldX*Math::Cos(planetRevolution)) + (oldZ*Math::Sin(planetRevolution)) ;
	float newZ = (oldX*-Math::Sin(planetRevolution)) + (oldZ*Math::Cos(planetRevolution));
	mNode->setPosition(newX, getPosition().y, newZ);
}

Vector3 Planet::getPosition()
{
	return mNode->getPosition();
}

Planet * Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}

Planet::Planet(SceneNode * node, std::string str){
	this->mNode = node;
	mName = str;
}

Planet* Planet::createPlanet(SceneManager& sceneManager, ManualObject* obj) {

	SceneNode* node = sceneManager.getRootSceneNode()->createChildSceneNode(obj->getName());


	node->attachObject(obj);

	return new Planet(node, obj->getName());
}
